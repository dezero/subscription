# Subscription module for Dz Framework

## Installation

Add these lines to composer.json file:

```shell
"require": {
    ...
    "dezero/subscription": "dev-master"
    ...
},
"repositories":[
    ...
    {
        "type": "vcs",
        "url" : "git@bitbucket.org:dezero/subscription.git"
    }
    ...
]
```

## Configuration

1) Define the module in the configuration file `/app/config/common/modules.php`
```shell

    // Subscription module
    'subscription' => [
        'class' => '\dzlab\subscription\Module',
        // 'class' => '\subscription\Module',
    ],
```

2) Add a new alias path in `/app/config/common/aliases.php`
```shell
    'dzlab.subscription'  => DZ_BASE_PATH . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'dezero' . DIRECTORY_SEPARATOR . 'subscription',
```

3) Set new component in `/app/config/common/components.php`
```shell
    // Subscription contrib component
    'subscriptionManager' => [
        'class' => '\dzlab\subscription\components\SubscriptionManager'
    ],
```