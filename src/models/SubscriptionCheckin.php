<?php
/**
 * @package dzlab\subscription\models 
 */

namespace dzlab\subscription\models;

use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\Log;
use dz\helpers\StringHelper;
use dzlab\subscription\models\_base\SubscriptionCheckin as BaseSubscriptionCheckin;
use dzlab\subscription\models\Subscription;
use user\models\User;
use Yii;

/**
 * SubscriptionCheckin model class for "subscription_checkin" database table
 *
 * Columns in table "subscription_checkin" available as properties of the model,
 * followed by relations of table "subscription_checkin" available as properties of the model.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property integer $checkin_id
 * @property integer $subscription_id
 * @property integer $user_id
 * @property string $subscription_type
 * @property integer $entity_id
 * @property string $entity_type
 * @property string $ip_address
 * @property integer $created_date
 * @property integer $created_uid
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 * @property mixed $createdUser
 * @property mixed $subscription
 * @property mixed $user
 */
class SubscriptionCheckin extends BaseSubscriptionCheckin
{
	/**
	 * Constructor
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	/**
	 * Returns the validation rules for attributes
	 */
	public function rules()
	{
		return [
			['subscription_id, user_id, subscription_type, created_date, created_uid', 'required'],
			['subscription_id, user_id, entity_id, created_date, created_uid', 'numerical', 'integerOnly' => true],
			['subscription_type, entity_type', 'length', 'max'=> 32],
			['ip_address', 'length', 'max'=> 255],
			['entity_id, entity_type, ip_address', 'default', 'setOnEmpty' => true, 'value' => null],
			['checkin_id, subscription_id, user_id, subscription_type, entity_id, entity_type, ip_address, created_date, created_uid', 'safe', 'on' => 'search'],
		];
	}
	

	/**
	 * Define relations with other objects
	 *
	 * There are four types of relations that may exist between two active record objects:
	 *   - BELONGS_TO: e.g. a member belongs to a team;
	 *   - HAS_ONE: e.g. a member has at most one profile;
	 *   - HAS_MANY: e.g. a team has many members;
	 *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
	 */
	public function relations()
	{
		return [
            'subscription' => [self::BELONGS_TO, Subscription::class, 'subscription_id'],
			'user' => [self::BELONGS_TO, User::class, ['user_id' => 'id']],
            'createdUser' => [self::BELONGS_TO, User::class, ['created_uid' => 'id']],

            // Custom relations
		];
	}


    /**
     * External behaviors
     */
    /*
    public function behaviors()
    {
        return \CMap::mergeArray([
            // Date format
            'DateBehavior' => [
                'class' => '\dz\behaviors\DateBehavior',
                'columns' => [
                    'disable_date' => 'd/m/Y - H:i'
                ],
            ],

        ], parent::behaviors());
    }
    */

	
	/**
	 * Returns the attribute labels
	 */
	public function attributeLabels()
	{
		return [
			'checkin_id' => Yii::t('app', 'Check-in'),
			'user_id' => Yii::t('app', 'User'),
			'subscription_type' => Yii::t('app', 'Subscription Type'),
            'entity_id' => Yii::t('app', 'Entity'),
            'entity_type' => Yii::t('app', 'Entity Type'),
			'ip_address' => Yii::t('app', 'Ip Address'),
			'created_date' => Yii::t('app', 'Created Date'),
			'created_uid' => null,
			'createdUser' => null,
            'subscription' => null,
			'user' => null
		];
	}


    /**
     * Generate an ActiveDataProvider for search form of this model
     *
     * Used in CGridView
     */
    public function search()
    {
        $criteria = new DbCriteria;
        
        $criteria->with = [];
        // $criteria->together = true;

        $criteria->compare('t.checkin_id', $this->checkin_id);
        $criteria->compare('t.subscription_id', $this->subscription_id);
        $criteria->compare('t.subscription_type', $this->subscription_type, true);
        $criteria->compare('t.entity_id', $this->entity_id);
        $criteria->compare('t.entity_type', $this->entity_type, true);
        $criteria->compare('t.ip_address', $this->ip_address, true);
        $criteria->compare('t.created_date', $this->created_date);

        return new \CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => ['pageSize' => 30],
            'sort' => ['defaultOrder' => ['checkin_id' => true]]
        ]);
    }


    /**
     * SubscriptionCheckin models list
     * 
     * @return array
     */
    public function subscriptioncheckin_list($list_id = '')
    {
        $vec_output = [];

        $criteria = new DbCriteria;
        $criteria->select = ['checkin_id', 'subscription_type'];
        // $criteria->order = 't.id ASC';
        // $criteria->condition = '';
        
        $vec_models = SubscriptionCheckin::model()->findAll($criteria);
        if ( !empty($vec_models) )
        {
            foreach ( $vec_models as $que_model )
            {
                $vec_output[$que_model->getAttribute('checkin_id')] = $que_model->title();
            }
        }

        return $vec_output;
    }
}