<?php
/**
 * @package dzlab\subscription\models 
 */

namespace dzlab\subscription\models;

use dz\db\DbCriteria;
use dz\helpers\DateHelper;
use dz\helpers\Log;
use dz\helpers\StringHelper;
use dzlab\subscription\models\_base\Subscription as BaseSubscription;
use dzlab\subscription\models\SubscriptionCheckin;
use user\models\User;
use Yii;

/**
 * Subscription model class for "subscription" database table
 *
 * Columns in table "subscription" available as properties of the model,
 * followed by relations of table "subscription" available as properties of the model.
 *
 * -------------------------------------------------------------------------
 * COLUMN FIELDS
 * -------------------------------------------------------------------------
 * @property integer $subscription_id
 * @property integer $user_id
 * @property string $subscription_type
 * @property string $email
 * @property integer $entity_id
 * @property string $entity_type
 * @property integer $start_date
 * @property integer $expired_date
 * @property integer $is_expired
 * @property integer $canceled_date
 * @property integer $canceled_uid
 * @property integer $is_canceled
 * @property string $data_json
 * @property integer $created_date
 * @property integer $created_uid
 * @property integer $updated_date
 * @property integer $updated_uid
 * @property string $uuid
 *
 * -------------------------------------------------------------------------
 * RELATIONS
 * -------------------------------------------------------------------------
 * @property mixed $canceledUser
 * @property mixed $createdUser
 * @property mixed $updatedUser
 * @property mixed $user
 * @property mixed $subscriptionCheckins
 */
class Subscription extends BaseSubscription
{
	/**
	 * Constructor
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	/**
	 * Returns the validation rules for attributes
	 */
	public function rules()
	{
		return [
			['user_id, subscription_type, created_date, created_uid, updated_date, updated_uid', 'required'],
			['user_id, entity_id, start_date, expired_date, is_expired, canceled_date, canceled_uid, is_canceled, created_date, created_uid, updated_date, updated_uid', 'numerical', 'integerOnly' => true],
			['subscription_type, entity_type', 'length', 'max'=> 32],
			['email', 'length', 'max'=> 255],
			['uuid', 'length', 'max'=> 36],
            ['data_json', 'safe'],
			['email, entity_id, entity_type, start_date, expired_date, is_expired, canceled_date, canceled_uid, is_canceled, data_json, uuid', 'default', 'setOnEmpty' => true, 'value' => null],
			['subscription_id, user_id, subscription_type, email, entity_id, entity_type, start_date, expired_date, is_expired, canceled_date, canceled_uid, is_canceled, data_json, created_date, created_uid, updated_date, updated_uid, uuid', 'safe', 'on' => 'search'],
		];
	}
	

	/**
	 * Define relations with other objects
	 *
	 * There are four types of relations that may exist between two active record objects:
	 *   - BELONGS_TO: e.g. a member belongs to a team;
	 *   - HAS_ONE: e.g. a member has at most one profile;
	 *   - HAS_MANY: e.g. a team has many members;
	 *   - MANY_MANY: e.g. a member has many skills and a skill belongs to a member.
	 */
	public function relations()
	{
		return [
            // BELONGS_TO relations
			'user' => [self::BELONGS_TO, User::class, 'user_id'],
            'canceledUser' => [self::BELONGS_TO, User::class, ['canceled_uid' => 'id']],
            'createdUser' => [self::BELONGS_TO, User::class, ['created_uid' => 'id']],
            'updatedUser' => [self::BELONGS_TO, User::class, ['updated_uid' => 'id']],

            // HAS_MANY relations
            'checkins' => [self::HAS_MANY, SubscriptionCheckin::class, 'subscription_id'],

            // Custom relations
            'firstCheckin' => [self::BELONGS_TO, SubscriptionCheckin::class, ['subscription_id' => 'subscription_id'], 'order' => 'firstCheckin.checkin_id ASC'],
            'lastCheckin' => [self::BELONGS_TO, SubscriptionCheckin::class, ['subscription_id' => 'subscription_id'], 'order' => 'lastCheckin.checkin_id DESC'],
		];
	}


    /**
     * External behaviors
     */
    public function behaviors()
    {
        return \CMap::mergeArray([
            // Date format
            'DateBehavior' => [
                'class' => '\dz\behaviors\DateBehavior',
                'columns' => [
                    'start_date' => 'd/m/Y - H:i',
                    'expired_date' => 'd/m/Y - H:i',
                    'canceled_date' => 'd/m/Y - H:i'
                ],
            ],

        ], parent::behaviors());
    }

	
	/**
	 * Returns the attribute labels
	 */
	public function attributeLabels()
	{
		return [
            'subscription_id' => Yii::t('app', 'Subscription'),
			'user_id' => Yii::t('app', 'User'),
			'subscription_type' => Yii::t('app', 'Subscription Type'),
			'email' => Yii::t('app', 'Email'),
			'entity_id' => Yii::t('app', 'Entity'),
			'entity_type' => Yii::t('app', 'Entity Type'),
			'start_date' => Yii::t('app', 'Start Date'),
			'expired_date' => Yii::t('app', 'Expired Date'),
			'is_expired' => Yii::t('app', 'Is Expired'),
			'canceled_date' => Yii::t('app', 'Canceled Date'),
			'canceled_uid' => null,
			'is_canceled' => Yii::t('app', 'Is Canceled'),
			'data_json' => Yii::t('app', 'Data Json'),
			'created_date' => Yii::t('app', 'Subscription Date'),
			'created_uid' => null,
			'updated_date' => Yii::t('app', 'Updated Date'),
			'updated_uid' => null,
			'uuid' => Yii::t('app', 'Uuid'),
			'canceledUser' => null,
			'createdUser' => null,
			'updatedUser' => null,
			'user' => null,
			'subscriptionCheckins' => null,
		];
	}


    /**
     * Generate an ActiveDataProvider for search form of this model
     *
     * Used in CGridView
     */
    public function search()
    {
        $criteria = new DbCriteria;
        
        $criteria->with = [];
        // $criteria->together = true;

        $criteria->compare('t.subscription_id', $this->subscription_id);
        $criteria->compare('t.subscription_type', $this->subscription_type);
        $criteria->compare('t.email', $this->email, true);
        $criteria->compare('t.entity_id', $this->entity_id);
        $criteria->compare('t.entity_type', $this->entity_type);

        /*
        $criteria->compare('t.start_date', $this->start_date);
        $criteria->compare('t.expired_date', $this->expired_date);
        $criteria->compare('t.is_expired', $this->is_expired);
        $criteria->compare('t.canceled_date', $this->canceled_date);
        $criteria->compare('t.is_canceled', $this->is_canceled);
        $criteria->compare('t.data_json', $this->data_json, true);
        $criteria->compare('t.created_date', $this->created_date);
        $criteria->compare('t.updated_date', $this->updated_date);
        $criteria->compare('t.uuid', $this->uuid, true);
        */

        return new \CActiveDataProvider($this, [
            'criteria' => $criteria,
            'pagination' => ['pageSize' => 30],
            'sort' => ['defaultOrder' => ['subscription_id' => true]]
        ]);
    }


    /**
     * Subscription models list
     * 
     * @return array
     */
    public function subscription_list($list_id = '')
    {
        $vec_output = [];

        $criteria = new DbCriteria;
        $criteria->select = ['subscription_id', 'subscription_type'];
        // $criteria->order = 't.id ASC';
        // $criteria->condition = '';
        
        $vec_models = Subscription::model()->findAll($criteria);
        if ( !empty($vec_models) )
        {
            foreach ( $vec_models as $que_model )
            {
                $vec_output[$que_model->getAttribute('subscription_id')] = $que_model->title();
            }
        }

        return $vec_output;
    }


    /**
     * Cancel a subscription
     */
    public function cancel()
    {
        if ( $this->is_canceled == 0 )
        {
            $this->scenario = 'cancel';
            $this->is_canceled = 1;
            $this->canceled_date = time();
            $this->canceled_uid = Yii::app()->user->id;

            if ( ! $this->save() )
            {
                Log::save_model_error($this);

                return false;
            }
        }

        return true;
    }


    /**
     * Reactivate a subscription
     */
    public function reactivate()
    {
        if ( $this->is_canceled == 1 )
        {
            $this->scenario = 'reactivate';
            $this->is_canceled = 0;
            $this->canceled_date = null;
            $this->canceled_uid = null;

            if ( ! $this->save() )
            {
                Log::save_model_error($this);

                return false;
            }
        }

        return true;
    }


    /**
     * Send email using a mailing template
     */
    public function send_email($template_mail, $recipient_email = null)
    {
        if ( $recipient_email !== null )
        {
            $recipient_email = $this->email;
        }
        
        $sending_result = Yii::app()->mail
            ->setTo($recipient_email)
            ->addModel($this->user, 'user')
            ->compose($template_mail)
            ->send();

        // Save error into LOG
        if ( ! $sending_result )
        {
            Yii::app()->mail->logError();
        }

        return $sending_result;
    }
}
