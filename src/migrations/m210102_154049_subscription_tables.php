<?php
/**
 * Migration class m210102_154049_subscription_tables
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;

class m210102_154049_subscription_tables extends Migration
{
	/**
	 * This method contains the logic to be executed when applying this migration.
	 */
	public function up()
	{
		// Create "subscription" table
        // -------------------------------------------------------------------------
        $this->dropTableIfExists('subscription', true);

        $this->createTable('subscription', [
            'subscription_id' => $this->primaryKey(),
            'user_id' => $this->integer()->unsigned()->notNull(),
            'subscription_type' => $this->string(32)->notNull(),
            'email' => $this->string(),
            'entity_id' => $this->integer()->unsigned(),
            'entity_type' => $this->string(32),
            'start_date' => $this->date(),
            'expired_date' => $this->date(),
            'is_expired' => $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(0),
            'canceled_date' => $this->date(),
            'canceled_uid' => $this->integer()->unsigned(),
            'is_canceled' => $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(0),
            'data_json' => $this->string(512),
            'created_date' => $this->date()->notNull(),
            'created_uid' => $this->integer()->unsigned()->notNull(),
            'updated_date' => $this->date()->notNull(),
            'updated_uid' => $this->integer()->unsigned()->notNull(),
            'uuid' => $this->uuid(),
        ]);
        
        // Create indexes
        $this->createIndex(null, 'subscription', ['user_id', 'subscription_type']);
        $this->createIndex(null, 'subscription', ['user_id', 'entity_id', 'entity_type'], false);
        $this->createIndex(null, 'subscription', ['entity_id', 'entity_type'], false);
        $this->createIndex(null, 'subscription', ['subscription_type'], false);

        // Create FOREIGN KEYS
        $this->addForeignKey(null, 'subscription', ['user_id'], 'user_users', ['id'], 'CASCADE', null);
        $this->addForeignKey(null, 'subscription', ['canceled_uid'], 'user_users', ['id'], 'CASCADE', null);
        $this->addForeignKey(null, 'subscription', ['created_uid'], 'user_users', ['id'], 'CASCADE', null);
        $this->addForeignKey(null, 'subscription', ['updated_uid'], 'user_users', ['id'], 'CASCADE', null);

        
        // Create "subscription_checkin" table
        // -------------------------------------------------------------------------
        $this->dropTableIfExists('subscription_checkin', true);

        $this->createTable('subscription_checkin', [
            'checkin_id' => $this->primaryKey(),
            'subscription_id' => $this->integer()->unsigned()->notNull(),
            'user_id' => $this->integer()->unsigned()->notNull(),
            'subscription_type' => $this->string(32)->notNull(),
            'entity_id' => $this->integer()->unsigned(),
            'entity_type' => $this->string(32),
            'ip_address' => $this->string(),
            'created_date' => $this->date()->notNull(),
            'created_uid' => $this->integer()->unsigned()->notNull()
        ]);

        // Create indexes
        $this->createIndex(null, 'subscription_checkin', ['user_id', 'subscription_type']);
        $this->createIndex(null, 'subscription_checkin', ['user_id', 'entity_id', 'entity_type'], false);

        // Create FOREIGN KEYS
        $this->addForeignKey(null, 'subscription_checkin', ['subscription_id'], 'subscription', ['subscription_id'], 'CASCADE', null);
        $this->addForeignKey(null, 'subscription_checkin', ['user_id'], 'user_users', ['id'], 'CASCADE', null);
        $this->addForeignKey(null, 'subscription_checkin', ['created_uid'], 'user_users', ['id'], 'CASCADE', null);

        // Permissions
        // -------------------------------------------------------------------------
        $this->insertMultiple('user_auth_item', [
            [
                'name'          => 'subscription.subscription.*',
                'type'          => 0,
                'item_type'     => 'operation',
                'description'   => 'Subscriptions - Subscription - Full access',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'subscription.subscription.create',
                'type'          => 0,
                'item_type'     => 'operation',
                'description'   => 'Subscriptions - Subscription - Create new subscriptions',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'subscription.subscription.update',
                'type'          => 0,
                'item_type'     => 'operation',
                'description'   => 'Subscriptions - Subscription - Edit subscriptions',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'subscription.subscription.view',
                'type'          => 0,
                'item_type'     => 'operation',
                'description'   => 'Subscriptions - Subscription - View subscriptions',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'subscription_manage',
                'type'          => 1,
                'item_type'     => 'task',
                'description'   => 'Subscriptions - Full access to subscriptions',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'subscription_edit',
                'type'          => 1,
                'item_type'     => 'task',
                'description'   => 'Subscriptions - Edit subscriptions',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'subscription_view',
                'type'          => 1,
                'item_type'     => 'task',
                'description'   => 'Subscriptions - View subscriptions',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
        ]);

        // Check-Ins
        $this->insertMultiple('user_auth_item', [
            [
                'name'          => 'subscription.checkin.*',
                'type'          => 0,
                'item_type'     => 'operation',
                'description'   => 'Subscriptions - Check-in - Full access',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'subscription.checkin.create',
                'type'          => 0,
                'item_type'     => 'operation',
                'description'   => 'Subscriptions - Check-in - Create new check-ins',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'subscription.checkin.update',
                'type'          => 0,
                'item_type'     => 'operation',
                'description'   => 'Subscriptions - Check-in - Edit check-ins',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'subscription.checkin.view',
                'type'          => 0,
                'item_type'     => 'operation',
                'description'   => 'Subscriptions - Check-in - View check-inss',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'checkin_manage',
                'type'          => 1,
                'item_type'     => 'task',
                'description'   => 'Subscriptions - Full access to check-ins',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'checkin_edit',
                'type'          => 1,
                'item_type'     => 'task',
                'description'   => 'Subscriptions - Edit check-ins',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
            [
                'name'          => 'checkin_view',
                'type'          => 1,
                'item_type'     => 'task',
                'description'   => 'Subscriptions - View check-ins',
                'created_date'  => time(),
                'uuid'          => StringHelper::UUID()
            ],
        ]);

        // Set items relationships
        $this->insertMultiple('user_auth_item_child', [
            // Subscriptions
            [
                'parent'    => 'subscription_manage',
                'child'     => 'subscription.subscription.*'
            ],
            [
                'parent'    => 'subscription_edit',
                'child'     => 'subscription.subscription.update'
            ],
            [
                'parent'    => 'subscription_edit',
                'child'     => 'subscription.subscription.view'
            ],
            [
                'parent'    => 'subscription_view',
                'child'     => 'subscription.subscription.view'
            ],
            [
                'parent'    => 'admin',
                'child'     => 'subscription_manage'
            ],
            [
                'parent'    => 'editor',
                'child'     => 'subscription_edit'
            ],

            // Check-ins
            [
                'parent'    => 'checkin_manage',
                'child'     => 'subscription.checkin.*'
            ],
            [
                'parent'    => 'checkin_edit',
                'child'     => 'subscription.checkin.update'
            ],
            [
                'parent'    => 'checkin_edit',
                'child'     => 'subscription.checkin.view'
            ],
            [
                'parent'    => 'checkin_view',
                'child'     => 'subscription.checkin.view'
            ],
            [
                'parent'    => 'admin',
                'child'     => 'checkin_manage'
            ],
            [
                'parent'    => 'editor',
                'child'     => 'checkin_edit'
            ],
        ]);

		return true;
	}


	/**
	 * This method contains the logic to be executed when removing this migration.
	 */
	public function down()
	{
		// $this->dropTable('my_table');
		return false;
	}
}

