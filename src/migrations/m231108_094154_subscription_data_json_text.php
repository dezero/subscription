<?php
/**
 * Migration class m231108_094154_subscription_data_json_text
 *
 * @link http://www.dezero.es/
 */

use dz\db\Migration;
use dz\helpers\DateHelper;
use dz\helpers\StringHelper;

class m231108_094154_subscription_data_json_text extends Migration
{
    /**
     * This method contains the logic to be executed when applying this migration.
     */
    public function up()
    {
        // Increase "data_json" column from VARCHAR(512) to TEXT
        $this->alterColumn('subscription', 'data_json', $this->text());

        return true;
    }


    /**
     * This method contains the logic to be executed when removing this migration.
     */
    public function down()
    {
        return false;
    }
}
